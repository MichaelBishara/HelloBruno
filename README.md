# HelloBruno - A Ultrasimple Java TTS Synthesizer project

Text To Speech (TTS) Synthesizer to be implented into a Rasberry Pi 3 B+ with the following additions: 

- Arch OS
- 60 or 65% Keyboard (Used as input)
- LED TFT Screen with large easily readable font glued to KB (Lead extension?)
- Battery (Must last several hours on while idle)
- 32Gb SD (OS/Software)
- Minimalistic/Light speakers

Software Used so far:

- Java 1.4
- FreeTTS 1.2.2

This project is for educational purposes - Computer Science student
