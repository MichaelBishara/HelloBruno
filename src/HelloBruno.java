import javax.speech.Central;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;

public class HelloBruno {

    static private String output = "";

    public static void main(String[] args)
        {

        //input argument args tostring to be used as output
        output = Arrays.toString(args);
        say(output);

        }

    private static void say(String textToOutput)
        {
            //!!!CREATE LOG FILE WITH JAR
            PrintWriter pw = null;
            try {
                pw = new PrintWriter(new FileWriter("log"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
            //set property as Kevin16 dictionary
            System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");

            //Register Engine
            Central.registerEngineCentral("com.sun.speech.freetts.jsapi.FreeTTSEngineCentral");

            //create a Synthesizer
            Synthesizer synthesizer = Central.createSynthesizer(new SynthesizerModeDesc(Locale.US));

            //allocate synthesizer
            synthesizer.allocate();

            //resume Synthesizer
            synthesizer.resume();

            //speaks the given text until queue is empty.
            synthesizer.speakPlainText(output, null);
            synthesizer.waitEngineState(Synthesizer.QUEUE_EMPTY);

            //deallocate the synthesizer
            synthesizer.deallocate();

        }
        catch (Exception e) {
            //prints errors to a log file
            Objects.requireNonNull(pw).println(e.getMessage());
        }



        }

        //TODO (2) Build on linux appropriate machine (Raspberry Pi app)
        //TODO (3) Add time and date to log
        //TODO (4) Change voice to easily understandable




}
